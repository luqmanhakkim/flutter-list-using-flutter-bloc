import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_list/model/post.dart';

abstract class PostState extends Equatable {
  const PostState();
}

///Initial State
class InitialPostState extends PostState {
  @override
  List<Object> get props => [];
}

///Render a loading indicator while initial batch of Post loaded
class PostUninitialized extends PostState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///Tells if got error while fetch Post
class PostError extends PostState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///Tells if got content to render
///posts will be List<Post> which will be display
///hasReachMax tells whether it has reach max number of posts
///Implement copyWith so that we can copy of instance of PostLoaded and update zero or more properties in future
///Overriding toString for an easier to read string representation of our event
class PostLoaded extends PostState {
  final List<Post> posts;
  final bool hasReachMax;

  const PostLoaded({this.posts, this.hasReachMax});

  PostLoaded copyWith({List<Post> posts, bool hasReachMax}) {
    return PostLoaded(
      posts: posts ?? this.posts,
      hasReachMax: hasReachMax ?? this.hasReachMax,
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [posts, hasReachMax];

  @override
  String toString() => 'PostLoaded{posts ${posts.length}, hasReachMax: $hasReachMax}';


}
