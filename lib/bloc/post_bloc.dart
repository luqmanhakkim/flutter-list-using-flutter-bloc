import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_bloc_list/bloc/post_event.dart';
import 'package:flutter_bloc_list/bloc/post_state.dart';
import 'package:flutter_bloc_list/model/post.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import './bloc.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final http.Client httpClient;

  PostBloc({@required this.httpClient});

  ///States the condition when list has reach its max
  bool _hasReachMax(PostState state) =>
      state is PostLoaded && state.hasReachMax;

  ///Calling api
  Future<List<Post>> _fetchPost(int startIndex, int limit) async {
    final response = await httpClient.get(
        'https://jsonplaceholder.typicode.com/posts?_start=$startIndex&_limit=$limit');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;

      return data.map((rawPost) {
        return Post(
            id: rawPost['id'], title: rawPost['title'], body: rawPost['body']);
      }).toList();
    } else {
      throw Exception('error fetch post');
    }
  }

  ///Debounce the events in order to prevent API spamming
  ///Overriding transform allows to transform the Stream before mapEventToState called
  @override
  Stream<PostState> transformEvents(Stream<PostEvent> events,
      Stream<PostState> Function(PostEvent event) next) {
    return super.transformEvents(
        events.debounceTime(Duration(milliseconds: 500)), next);
  }

  @override
  PostState get initialState => PostUninitialized();

  @override
  Stream<PostState> mapEventToState(
    PostEvent event,
  ) async* {
    final currentState = state;
    if (event is Fetch && !_hasReachMax(currentState)) {
      try {
        if (currentState is PostUninitialized) {
          final posts = await _fetchPost(0, 20);

          yield PostLoaded(posts: posts, hasReachMax: false);
          return;
        }
        if (currentState is PostLoaded) {
          final posts = await _fetchPost(currentState.posts.length, 20);
          yield posts.isEmpty
              ? currentState.copyWith(hasReachMax: true)
              : PostLoaded(
                  posts: currentState.posts + posts,
                  hasReachMax: false,
                );
        }
      } catch (_) {
        yield PostError();
      }
    }
  }
}
