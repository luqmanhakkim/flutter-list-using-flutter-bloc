import 'package:equatable/equatable.dart';

abstract class PostEvent extends Equatable {

  @override
  List<Object> get props => [];
}

///Fetch used to load whenever it needs more Post to present
///Since the apps only shows data only one event occur
class Fetch extends PostEvent{}
