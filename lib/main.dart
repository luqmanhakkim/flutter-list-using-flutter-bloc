import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_list/bloc/post_bloc.dart';
import 'package:flutter_bloc_list/bloc/post_event.dart';
import 'package:flutter_bloc_list/bloc_delegate.dart';
import 'package:flutter_bloc_list/model/homepage.dart';
import 'package:http/http.dart' as http;

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter List',
      home:Scaffold(
        appBar: AppBar(
          title: Text('List'),
        ),
        body: BlocProvider(
          create: (context) => PostBloc(httpClient: http.Client())..add(Fetch()),
          child: HomePage(),
        ),
      )
    );
  }
}

